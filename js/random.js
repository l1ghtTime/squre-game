const getRandomColor = (min = 0, max = 254) => {
    return Math.floor(Math.random() * (max - min) + min);
};

export default getRandomColor;
