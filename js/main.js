import random from '../js/random.js';

const board = document.querySelector('.content__board');
const points = document.querySelector('.navigation-point__value');
const actionBtn = document.querySelector('.action-btn');
const newGame = document.querySelector('.new-game');
const resultBoard = document.querySelector('.content__result-board');
const timeLeft = document.querySelector('.time-left');
const finishScore = document.querySelector('.finish-results__score');
const userName = document.querySelector('.finish-results__name');
const scoreBtn = document.querySelector('.finish-results__btn');
const tableResult = document.querySelector('.content__results');
let timeLeftValue = +timeLeft.innerHTML;
let pointsValue = document.querySelector('.navigation-point__value').innerHTML;
let interval = null;
let results = [];

(function createBoard(){
    const squre = document.createElement('div');
    squre.classList.add('squre');
    board.appendChild(squre);

    if(board.childNodes.length < 100) {
        createBoard();
    }
})();

function randomSqure() {
    return board.childNodes[`${random(0, 99)}`];
}

function createNewSqure() {
    const newSqure = document.createElement('div');
    newSqure.classList.add('start-squre');
    newSqure.style.backgroundColor = `rgb( ${random()}, ${random()}, ${random()} )`;
    return newSqure;
}

const startRandomSqure = [randomSqure(), randomSqure(), randomSqure()];

startRandomSqure.map(item => {
    item.appendChild(createNewSqure());    
});

function squreActions(event) {
    let parent;
    event.target.classList.contains('start-squre') ? parent = event.target.parentElement.parentElement : parent = event.target.parentElement;

    const emptySqures = [...parent.childNodes].filter(item => !item.childNodes.length ? item : null);

    if(event.target.classList.contains('start-squre')) {
        
        event.target.remove();
        for(let i = 0; i <= `${random(0, 2)}`; i++) {
            let emptySquresCount = emptySqures.length;
            let randomDOM = emptySqures[`${random(0, emptySquresCount)}`];
            randomDOM.appendChild(createNewSqure());
        }

        pointsValue = +pointsValue;
        points.innerHTML = (++pointsValue);
    }
}

function resetGame() {
    timeLeft.innerHTML = '60';
    timeLeftValue = 60;
    points.innerHTML = 0;
    pointsValue = 0;
    actionBtn.innerHTML = 'Start';
    actionBtn.classList.remove('active');
    newGame.classList.remove('active');
}

actionBtn.addEventListener('click', event => {

    event.target.classList.toggle('active');
    event.target.classList.contains('active') ? event.target.innerHTML = 'Stop' : event.target.innerHTML = 'Start';  

    if(event.target.innerHTML === 'Start') {
        clearInterval(interval);
        interval = null;
        board.removeEventListener('click', squreActions);

        if(newGame.classList.contains('active')) {
            newGame.classList.remove('active');
        }
    }

    event.target.innerHTML === 'Stop' ? interval = setInterval(() => {

        board.addEventListener('click', squreActions);
        timeLeftValue--;
        timeLeft.innerHTML = timeLeftValue;


        if(newGame.classList.contains('active')) {
            clearInterval(interval);
        } 

        if(timeLeftValue  < 1) {
            board.removeEventListener('click', squreActions);
            clearInterval(interval);
            finishScore.innerHTML = points.innerHTML;

            resultBoard.classList.add('results-show');
            resultBoard.classList.add('animate__backInLeft')

            if(resultBoard.classList.contains('animate__backOutLeft')) {
                resultBoard.classList.remove('animate__backOutLeft')
            }
        }
    }, 1000) : null;

});

newGame.addEventListener('click', event => {
    resetGame();

    [...board.childNodes].filter(item => item.childNodes.length ? item.childNodes[0].remove() : null);

    const newRandomSqure = [randomSqure(), randomSqure(), randomSqure()];

    newRandomSqure.map(item => {
        item.appendChild(createNewSqure());    
    });
    

    if(resultBoard.classList.contains('animate__backInLeft')) {
        resultBoard.classList.add('animate__backOutLeft')
        resultBoard.classList.remove('animate__backInLeft');
    }
});

scoreBtn.addEventListener('click', event => {
    resetGame();
    board.removeEventListener('click', squreActions);
    
    results.push({
        name: userName.value,
        score: finishScore.innerHTML
    });

    userName.value = '';

    if(resultBoard.classList.contains('animate__backInLeft')) {
        resultBoard.classList.add('animate__backOutLeft')
        resultBoard.classList.remove('animate__backInLeft');
    }

    resultsTable(results);
});

function resultsTable(results) {
    let resultTable = document.querySelector('.all-results');
    resultTable.innerHTML = '';

    results.map(score => {
        let result = document.createElement('div');
        result.classList.add('table-result');
        result.innerHTML = `<span class="table-result__name">${score.name}</span><span class="table-result__score">${score.score}<span>`;
        resultTable.appendChild(result);
    });
}